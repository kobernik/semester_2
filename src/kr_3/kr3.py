import random
import math
import matplotlib.pyplot as plt
import numpy as np

mass_point_graph, mass_pi_graph = [], []

log_in = 0  # переменная для подсчета точек
points_step = 10  # шаг увеличения кол-ва точек

# Создание рандомных точек в кол-ве от 10 до 1000000
for i in range(10, 1000000, points_step):
    for _ in range(points_step):
        if math.sqrt(random.uniform(0, 1) ** 2 + random.uniform(0, 1) ** 2) <= 1:
            log_in += 1

    # Добавление в массивы кол-во точек и подсчитанное значение числа pi
    mass_point_graph.append(i)
    mass_pi_graph.append(4 * log_in / i)

# Массив реального числа pi
mass_real_pi = [np.pi for _ in range(len(mass_point_graph))]

# Построение графика
plt.plot(mass_point_graph, mass_pi_graph, label='Подсчитанное')
plt.plot(mass_point_graph, mass_real_pi, label='Реальное')
plt.title('Изменение значения числа Pi')
plt.xlabel('Кол-во точек')
plt.ylabel('Значение Pi')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Значения', title_fontsize='8')
plt.grid()
plt.show()
