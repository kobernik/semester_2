from src.kr_1 import kr1
import matplotlib.pyplot as plt

a, x1, x2 = 3, 0, 100  # параметры для функции
real_val = 885  # 884.548987101228
qnt_step_start, qnt_step_stop, step = 4, 101, 4  # кол-во шагов при интегрировании

# Заполнение массива с реальным значением интегрирования функции
mass_real_val = [real_val for i in range(qnt_step_start, qnt_step_stop, step)]

# Заполнение массивов с подсчитанными данными интегрирования функции
mass_val_rectangle = [kr1.rectangle(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]
mass_val_trapezoid = [kr1.trapezoid(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]
mass_val_simpson = [kr1.simpson(a, x1, x2, i) for i in range(qnt_step_start, qnt_step_stop, step)]

# Заполнение массива с кол-вом шагов при интегрировании
mass_qnt_step = [i for i in range(qnt_step_start, qnt_step_stop, step)]

# Построение графика
ax = plt.subplot()
plt.plot(mass_qnt_step, mass_real_val, label='Реальное значение')
plt.plot(mass_qnt_step, mass_val_rectangle, marker='o', ms=3, label='Метод прямоугольников')
plt.plot(mass_qnt_step, mass_val_trapezoid, marker='o', ms=3, label='Метод трапеций')
plt.plot(mass_qnt_step, mass_val_simpson, marker='o', ms=3, label='Метод Симпсона')
plt.title('Точность интегрирования относительно реального значения')
plt.xlabel('Кол-во шагов при интегрировании')
plt.ylabel('Значение интегрирования')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
ax.set_xlim(mass_qnt_step[0], mass_qnt_step[-1])
plt.grid()
plt.show()
